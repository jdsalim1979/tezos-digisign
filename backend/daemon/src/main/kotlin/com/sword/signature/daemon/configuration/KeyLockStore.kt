package com.sword.signature.daemon.configuration

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantLock

@Component
class KeyLockStore {

    private val concurrentLocks = ConcurrentHashMap<String, ReentrantLock>()

    @Synchronized
    fun getLock(key: String): ReentrantLock {
        LOGGER.trace("Retrieving lock for key '{}'", key)
        return concurrentLocks.getOrPut(key) { ReentrantLock() }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(KeyLockStore::class.java)
    }
}
