[Index](../README.md) | [Architecture](./architecture.md) | [Smart contract](../contract/README.md) | [Docker Deployment](./docker-deployment.md) | [Quick Start](./quickstart.md) | [Keys Management](./keys-management.md)

# Docker Deployment

## Deployment options

### Sandbox node

Components:
* MongoDB database
* MongoDB administration UI
* FakeSMTP server
* Tezos sandbox node
* Tzindex block indexer
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/sandbox/application-daemon.yml**.
* REST server configuration file: **compose-config/sandbox/application-rest.yml**.

### Delphinet mode

Components:
* MongoDB database
* MongoDB administration UI
* FakeSMTP server
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/delphinet/application-daemon.yml**.
* REST server configuration file: **compose-config/delphinet/application-rest.yml**.
* To add your tezos keys: edit the daemon configuration (property *tezos.keys.admin*).

### Mainnet mode

Components:
* MongoDB database
* MongoDB administration UI
* FakeSMTP server
* Tezos Signature REST server (+ client)
* Tezos Signature daemon

Configuration:
* Daemon configuration file: **compose-config/mainnet/application-daemon.yml**.
* REST server configuration file: **compose-config/mainnet/application-rest.yml**.
* To add your tezos keys: edit the daemon configuration (property *tezos.keys.admin*).
  
The only difference between **Carthagenet mode** and **Mainnet mode** is the configuration (node URL, smart contract address, tezos keys).

## Deploy the solution

### Docker images build (*either build or import*)

* Clean gradlew cache (if necessary)
```
./gradlew clean
```

* Build and unpack all services
```
./gradlew unpack
```

* Build the images (after each code update)

#### Sandbox

```
docker-compose -f docker-compose.sandbox.yml build
```

#### Delphinet

```
docker-compose -f docker-compose.delphinet.yml build
```

#### Mainnet

```
docker-compose -f docker-compose.mainnet.yml build
```

### Docker images import (*either build or import*)

* Import the docker images of each provided service
```
docker load -i TAR_FILE
```

### Run the services

#### Sandbox

* Run the services
```
docker-compose -f docker-compose.sandbox.yml up -d
```
* The same way as development mode, for sandbox you need to originate the contract.
```
(cd contract; make originate)
```
* Then, edit the file **compose-config/sandbox/application-daemon.yml** and replace the property *tezos.contract.address* by the contract address.
* Do the same update to the file **compose-config/sandbox/application-rest.yml**.
* Finally, restart the daemon and rest containers:
```
docker restart tezos-digisign-daemon tezos-digisign-rest
```

#### Delphinet

* Run the services
```
docker-compose -f docker-compose.delphinet.yml up -d
```

#### Mainnet:

* Run the services
```
docker-compose -f docker-compose.mainnet.yml up -d
```

### Stop the services

#### Sandbox

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.sandbox.yml down
```

#### Delphinet

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.delphinet.yml down
```

#### Mainnet

* Stop the services (add **-v** to reset database)
```
docker-compose -f docker-compose.mainnet.yml down
```

### Access the services

* Access the UI at `http://localhost:9090` (default credentials: admin/Sword@35)
* Access the REST API documentation at `http://localhost:9090/swagger-ui.html`
* Access the MongoDB UI at `http://localhost:8081`
* To authenticate on the REST API: click on the **Authorize** button and provide the access token (retrieved through the UI)

### Recover the database from a backup

By default, the backup container will create daily backup and stored them in a docker volume or filesystem directory.
The backup files naming convention is **backup-yyyyMMdd_HHmmss.tar.gz**.

In order to import a backup, you need to mount the backup as a docker volume for the database container and then execute the mongorestore command within the database container context.

**Warning** : be careful not to have launched Digisign daemon before importing the backup. Otherwise the database will already be initialized and you will have issues with duplicated migrations. The database needs to be empty for the backup to be restored without any error occuring afterwards.

#### Recovery example with docker compose

* Add the backup as volume.
```yaml
services:
  mongo:
    ...
    volumes:
      - 'mongo-data:/data/db'
      - './backup-yyyyMMdd_HHmmss.tar.gz:/backup.tar.gz'
```

* Launch only the database with docker-compose
```shell
docker-compose -f docker-compose.ENV.yml up -d mongo
```

* Access the database container shell
```shell
docker exec -it tezos-digisign-mongo bash
```

* Restore the backup within the database container context
```shell
mongorestore --uri mongodb://root:password@localhost:27017 --gzip --archive=/backup.tar.gz
```

* Exit the container (Ctrl+D or *exit*)

* Launch all the services with docker-compose
```shell
docker-compose -f docker-compose.ENV.yml up -d
```
