package com.sword.signature.api.custom


data class OptionalFeatures(
    val registering: Registering
) {

    // Features
    data class Registering(
        val enabled: Boolean
    )

}


