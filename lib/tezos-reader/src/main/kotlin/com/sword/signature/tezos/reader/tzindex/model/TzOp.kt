package com.sword.signature.tezos.reader.tzindex.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class TzOp(
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("block")
    val block: String,
    @JsonProperty("time")
    val time: OffsetDateTime,
    @JsonProperty("height")
    val height: Long,
    @JsonProperty("big_map_diff")
    val bigMapDiff: List<BigMapDiff>,
    @JsonProperty("receiver")
    val contract: String
) {
    data class BigMapDiff(
        @JsonProperty("key")
        val key: String,
        @JsonProperty("key_hash")
        val keyHash: String,
        @JsonProperty("value")
        val value: Value,
        @JsonProperty("action")
        val action: String
    ) {
        data class Value(
            @JsonProperty("0@timestamp")
            val timestamp: OffsetDateTime,
            @JsonProperty("1@address")
            val address: String
        )
    }
}
