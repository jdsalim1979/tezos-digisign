package com.sword.signature.rest.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "optional-features")
class OptionalFeaturesConfig {

    var registering: Registering = Registering()

    class Registering {
        var enabled: Boolean = false
        var defaultSignatureLimit: Int = 10
        var defaultHash: String = ""
        var defaultPublicKey: String = ""
    }
}
